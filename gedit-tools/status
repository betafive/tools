#!/bin/sh
# [Gedit Tool]
# Input=nothing
# Output=output-panel
# Name=status
# Applicability=local
# Save-files=document

################################################################################
#   status
#
#   gedit tool to get status information from git or mercurial
#
#   Copyright (C) 2013 Paul Barker
#
#   Permission is hereby granted, free of charge, to any person obtaining a
#   copy of this software and associated documentation files (the "Software"),
#   to deal in the Software without restriction, including without limitation
#   the rights to use, copy, modify, merge, publish, distribute, sublicense,
#   and/or sell copies of the Software, and to permit persons to whom the
#   Software is furnished to do so, subject to the following conditions:
#
#   The above copyright notice and this permission notice shall be included in
#   all copies or substantial portions of the Software.
#
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
################################################################################
set -e

# Determine repository format and end up at the top level directory
cd "$GEDIT_CURRENT_DOCUMENT_DIR"
FORMAT=
while true; do
    if [ -d ".git" ]; then
        FORMAT=git
        break
    elif [ -d ".hg" ]; then
        FORMAT=hg
        break
    elif [ `pwd` = "/" ]; then
        echo "No repository found!" >&2
        exit 1
    fi
    cd ..
done

case $FORMAT in
    hg)
        hg status
        ;;
    git)
        git status
        ;;
    *)
        echo "Unrecognised repository format!" >&2
        exit 1
        ;;
esac
